import { expect, test } from '@playwright/test'

import { PetApis } from '../../api/pet_api'

import { petData } from '../../testData/apiData/petCreationData.json';
import { uri } from '../../testData/routings.json';

const editJsonFile = require("edit-json-file");
const file = editJsonFile(`testData/routings.json`);
test.describe.serial('pet crud ations', () => {

    test('validate pet creation with valid data', async ({ request, baseURL }) => {
        const pet_api = new PetApis(request, baseURL);

        const _response = await pet_api.createNewPet(petData[0]);
        
        expect(await _response.status()).toBe(200);
        const response = await _response.text();
        await file.set("uri.petId", await response.substring(response.indexOf('id\":') + 4, response.indexOf(',\"category')));
        await file.save();
        const resJson = await _response.json();
        expect(await resJson.name).toBe(petData[0].name);
    });

    test('validate get pet details by pet Id', async ({ request, baseURL }) => {
        const pet_api = new PetApis(request, baseURL);

        const _response = await pet_api.getCreatedPet(uri.petId);

        expect(_response.status()).toBe(200);
        const response = await _response.json();
        expect(response.name).toBe(petData[0].name);
    });

    test('validate update pet details by pet Id', async ({ request, baseURL }) => {
        const pet_api = new PetApis(request, baseURL);

        petData[0].id = uri.petId;
        petData[0].category.id = "1111";
        petData[0].category.name = "Fish";
        petData[0].name = "Gold Fish";

        const _response = await pet_api.updateCreatedPet(petData[0]);

        expect(_response.status()).toBe(200);
        const response = await _response.json();
        expect(response.name).toBe("Gold Fish");
        expect(response.category.id).toBe(1111);
        expect(response.category.name).toBe("Fish");
    });

    test('validate delete pet details by pet Id', async ({ request, baseURL }) => {
        const pet_api = new PetApis(request, baseURL);

        let _response = await pet_api.deleteCreatedPet(uri.petId);

        expect(_response.status()).toBe(200);
        let response = await _response.json();
        expect(response.code).toBe(200);
        response = await _response.text();
        expect(response).toContain(uri.petId);

        _response = await pet_api.getCreatedPet(uri.petId);

        expect(_response.status()).toBe(404);
        response = await _response.json();
        expect(response.code).toBe(1);
        expect(response.type).toBe("error");
        expect(response.message).toBe("Pet not found");
    });
});