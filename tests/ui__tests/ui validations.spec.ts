import test, { expect } from "@playwright/test";

import { sections } from '../../testData/sections.json';
import { routings } from '../../testData/routings.json'

import { LandingPage } from "../../pom/LandingPage";
import { ClassAttributePage } from "../../pom/ClassAttributePage";
import { InputTextPage } from "../../pom/InputTextPage";
import { ClickPage } from "../../pom/ClickPage";

test.describe.parallel('web ui test scenarios', () => {
    test('validate primary (blue) button click and press ok in alert popup function', async ({ page, baseURL }) => {

        const landingPage = new LandingPage(page);
        const classAttributePage = new ClassAttributePage(page);

        await landingPage.goToLandingPage(baseURL);
        await landingPage.navigateToSection(sections.class_attribute);
        await classAttributePage.validateLandingPage(baseURL+routings.classAttribute, sections.class_attribute);
        await classAttributePage.verifyClickPrimaryButtonForAlert("Primary button pressed");
    });

    test('validate setting text into the input field and pressing the button function', async ({ page, baseURL }) => {

        const landingPage = new LandingPage(page);
        const inputTextPage = new InputTextPage(page);

        await landingPage.goToLandingPage(baseURL);
        await landingPage.navigateToSection(sections.text_input);
        await inputTextPage.validateLandingPage(baseURL+routings.textInput, sections.text_input);
        await inputTextPage.enterTextValueAndPerformClick("button name changed");
        await inputTextPage.validateChangedButtonText("button name changed");
    });

    test('validate button click. The button should change to green after clicking', async ({ page, baseURL }) => {

        const landingPage = new LandingPage(page);
        const clickPage = new ClickPage(page);

        await landingPage.goToLandingPage(baseURL);
        await landingPage.navigateToSection(sections.click);
        await clickPage.validateLandingPage(baseURL+routings.click, sections.click);
        await clickPage.performMouseClick();
        await clickPage.validateButtonChange();
    });
})