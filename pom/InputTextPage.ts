import { expect, Page } from "@playwright/test";
import { BasePage } from "../utils/BasePage";

export class InputTextPage {

    private basePage: BasePage;

    private lblHeader: string = '//h3';
    private txtInput: string = '#newButtonName';
    private btnUpdating: string = '#updatingButton';

    constructor(page: Page) {
        this.basePage = new BasePage(page);
    }

    async validateLandingPage(url: string, headerText: string) {
        const currentUrl = await this.basePage.getCurrentUrl();
        const header = await this.basePage.getElementText(this.lblHeader);
        expect(currentUrl).toEqual(url);
        expect(header).toEqual(headerText);
    }

    async enterTextValueAndPerformClick(inputText: string) {
        await this.basePage.clickElement(this.txtInput)
        await this.basePage.enterValueFromKeyBoard(inputText);
        await this.basePage.pressKeyboard("Tab");
        await this.basePage.pressKeyboard("Enter");
    }

    async validateChangedButtonText(buttonText: string) {
        expect(await this.basePage.getElementText(this.btnUpdating)).toEqual(buttonText);
    }
}