import { Dialog, expect, Page } from "@playwright/test";
import { BasePage } from "../utils/BasePage";

export class ClassAttributePage {

    private basePage: BasePage;

    private btnPrimaryButton: string = "//button[contains(concat(' ', normalize-space(@class), ' '), ' btn-primary ')]";
    private lblHeader: string = '//h3';

    constructor(page: Page) {
        this.basePage = new BasePage(page);
    }

    async validateLandingPage(url: string, headerText: string) {
        const currentUrl = await this.basePage.getCurrentUrl();
        const header = await this.basePage.getElementText(this.lblHeader);
        expect(currentUrl).toEqual(url);
        expect(header).toEqual(headerText);
    }

    async verifyClickPrimaryButtonForAlert(message: string) {
        const dialogInfo = await this.basePage.getDialogBoxInfo();
        await this.basePage.clickElement(this.btnPrimaryButton);
        expect(dialogInfo.type).toBe('alert');
        expect(dialogInfo.message).toBe(message);
    }
}