import { expect, Page } from '@playwright/test'
import { BasePage } from '../utils/BasePage';

export class LandingPage {

    private basePage: BasePage;

    private linkDynamicId: string = 'a[href="/dynamicid"]';
    private linkClassAttribute: string = 'a[href="/classattr"]';
    private linkCick: string = 'a[href="/click"]';
    private linkTextInput: string = 'a[href="/textinput"]';

    constructor(page: Page) {
        this.basePage = new BasePage(page);
    }

    async goToLandingPage(baseUrl: string) {
        await this.basePage.goToUrl(baseUrl);
    }

    async navigateToSection(sectionName: string) {
        switch (sectionName) {
            case 'Dynamic ID':
                await this.basePage.clickElement(this.linkDynamicId);
                break;
            case 'Class Attribute':
                await this.basePage.clickElement(this.linkClassAttribute);
                break;
            case 'Click':
                await this.basePage.clickElement(this.linkCick);
                break;
            case 'Text Input':
                await this.basePage.clickElement(this.linkTextInput);
                break;
        }
    }
}