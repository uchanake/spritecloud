import { expect, Page } from "@playwright/test";
import { BasePage } from "../utils/BasePage";

export class ClickPage {

    private basePage: BasePage;

    private lblHeader: string = '//h3';
    private btnBad: string = '[class="btn btn-primary"]';
    private btnSuccess: string = '[class="btn btn-success"]';

    constructor(page: Page) {
        this.basePage = new BasePage(page);
    }

    async validateLandingPage(url: string, headerText: string) {
        const currentUrl = await this.basePage.getCurrentUrl();
        const header = await this.basePage.getElementText(this.lblHeader);
        expect(currentUrl).toEqual(url);
        expect(header).toEqual(headerText);
    }

    async performMouseClick() {
        await this.basePage.mouseHoverAndClick(this.btnBad);
    }

    async validateButtonChange() {
        expect(await this.basePage.isElementVisible(this.btnBad)).toBe(false);
        expect(await this.basePage.isElementVisible(this.btnSuccess)).toBe(true);
        expect(await this.basePage.getElementColor(this.btnSuccess)).toBe("rgb(33, 136, 56)");
    }

    async validateButtonColorChange() {
    }
}