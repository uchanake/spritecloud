import { Locator, Page, selectors } from "@playwright/test";

export class BasePage {

    private page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async actionLocateElement(locator: string) {
        const element = await this.page.locator(locator);
        return element;
    }

    async goToUrl(baseUrl: string) {
        if (baseUrl) {
            await this.page.goto(baseUrl);
            await this.page.waitForLoadState("networkidle");
        } else {
            throw new Error("Error : base URL not found..!");
        }
    }

    async clickElement(selector: string) {
        const element = await this.actionLocateElement(selector);
        await element.waitFor({ state: "visible" });
        await element.click();
    }

    async getDialogBoxInfo() {
        type Dialog = {
            type?: string,
            message?: string
        };

        const info: Dialog = {}
        await this.page.on('dialog', async (dialog) => {
            info.type = await dialog.type();
            info.message = await dialog.message();
            dialog.accept();
        });
        return info;
    }

    async getCurrentUrl() {
        const url = await this.page.url();
        return url;
    }

    async getElementText(selector: string) {
        const element = await this.actionLocateElement(selector);
        const text = await element.textContent();
        return text;
    }

    async enterValueFromKeyBoard(inputText: string) {
        await this.page.keyboard.type(inputText);
    }

    async pressKeyboard(key: string) {
        await this.page.keyboard.press(key);
    }

    async mouseHoverAndClick(selector: string) {
        const element = await this.actionLocateElement(selector);
        await element.hover();
        await this.page.mouse.down();
        await this.page.mouse.up();
        await this.page.waitForTimeout(2000);
    }

    async isElementVisible(selector: string) {
        const element = await this.actionLocateElement(selector);
        return await element.isVisible();
    }

    async getElementColor(selector: string) {
        const element = await this.actionLocateElement(selector);
        const color = await element.evaluate(async (ele) => {
            return await window.getComputedStyle(ele).getPropertyValue("background-color");
        });
        return await color;
    }
}