
# spriteCloud Automation Assignment

## Description

This automation Project is implemented using following tools and technologies
1. TypeScript
2. Playwright
3. GitLab for CI integration

## Automation Scope
1. API Automation : https://petstore.swagger.io/
2. Web Automation : http://www.uitestingplayground.com/


## Test Execution Instructions (Running tests in locally)
**Pre-condition**: Node 10+

Follow the steps below to execute tests
1. Clone the project from https://gitlab.com/uchanake/spritecloud
2. Open project location with VSCODE
3. Enter in vscode terminal `npm install`
4. Enter in vscode terminal `npx playwright install`
5. To run the tests
    1. API Tests: `STAG_API_URL=https://petstore.swagger.io npx playwright test --project=chromium`
    2. UI Tests: `STAG_UI_URL=http://www.uitestingplayground.com/ npx playwright test --project=api_tests`
    3. Run All Tests: `STAG_API_URL=https://petstore.swagger.io STAG_UI_URL=http://www.uitestingplayground.com/ npx playwright test --project=chromium --project=api_tests`

   Wait for the  ✨Magic ✨

Test will run on Chrome browser, on parallel(UI Tests) and on series(API Tests) by default. Update playwright config if you need to run on other browsers.

## Test cases
**Web ui tests**
1. validate primary (blue) button click and press ok in alert popup function
2. validate setting text into the input field and pressing the button function
3. validate button click. The button should change to green after clicking

**API tests**
1. validate pet creation with valid data
2. validate get pet details by pet Id
3. validate update pet details by pet Id
4. validate delete pet details by pet Id


## Running in GitLab - CI integration

CI build will start with each merge to the master automatically, which will execute the automated test cases. Also the pipeline can be triggered manually. Follow steps below to view the CI pipline

Report Upload job will be triggered manually based on the requrement. To do it, follow the below steps
1. Navigate to https://gitlab.com/uchanake/spritecloud/-/pipelines
2. Click on the latest(or pending report upload build) and click play button for “report upload”

## Test Run Reports 
1. Once the execution completed and make it as public by the owner(Chanaka), you will be able to see the report from https://app.calliope.pro/reports/153606/public/7eb9cc22-5d9e-4a05-8e61-d3448d04877e

## Calliope.pro
**Improvement**
* Error message:`invalid date`
Getting above error message for junit xml reports and reason is a date validation failure in internal parameters json. This is working when the report upload manually, but not through the API call. There should be an easy mechanism to report those issues to the support team and need the acknowledgement for the reported issue for better user experience. Chatbot or live support did not respond to when this issue raised on the chat.

**New Feature**
* Sharing the latest result is impossible. New feature required for generate a link to share the latest result uploaded.

## What you used to select the scenarios, what was your approach?
**For UI tests**
1. Effort to automate
    - Focused on the effort for automate (time to complete automation)
3. Execution frequency
    - Focused on most frequently using scenarios in day to day testing cycle
5. Common actions
    - Focused on most commonly using scenarios in day to day testing cycle
6. Impact of functionality 
    - Focused on the scenarios that can be blocked the testing (Dynamic element ID will not blocked the testing)

## Why are they the most important 
1. ROI - More test cases in less time will help to reduce the manual test effort effectively. That saving time can be used for automate complex areas
2. Execution frequency - Less frequently executing test cases are less priority to automate
3. Common actions - Common action can be helpful for code reusability
4. Manual testing will not be blocked for some functionalities are broken. Those areas are less priority as less impact to the functionality and the system

## Next step
1. Improve the falky tests to be solid
2. Compatibility tests improvements
3. Framework extend with more fetures
