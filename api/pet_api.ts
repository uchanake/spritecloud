import { APIRequestContext } from "@playwright/test";

import { uri } from '../testData/routings.json';

export class PetApis {

    private request: APIRequestContext;
    private baseUrl: string;

    constructor(request: APIRequestContext, baseUrl: string) {
        this.request = request;
        this.baseUrl = baseUrl;
    }

    async createNewPet(petDetails) {
        const _response = await this.request.post(this.baseUrl + uri.pet, {
            data: petDetails,
            headers: {
                "Content-Length": "",
                "Content-Type": "application/json"
            }
        });
        return _response;
    }

    async getCreatedPet(petId) {
        const _response = await this.request.get(this.baseUrl + uri.pet + petId, {
            headers: {
                "accept": "application/json"
            }
        });
        return _response;
    }

    async updateCreatedPet(petDetails) {
        const _response = await this.request.put(this.baseUrl + uri.pet, {
            data: petDetails,
            headers: {
                "Content-Length": "",
                "Content-Type": "application/json"
            }
        });
        return _response;
    }

    async deleteCreatedPet(petId) {
        const _response = await this.request.delete(this.baseUrl + uri.pet + petId, {
            headers: {
                "api_key":"special-key"
            }
        });
        return _response;
    }
}