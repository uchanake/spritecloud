import type { PlaywrightTestConfig } from '@playwright/test';
import { devices } from '@playwright/test';

const config: PlaywrightTestConfig = {
  testDir: './tests',
  timeout: 30 * 1000,
  expect: {
    timeout: 5000
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: [
    ['junit', { outputFile: 'playwright-report/test-results.xml' }],
    ['json', { outputFile: 'playwright-report/mochawesome-test-results.json' }]
  ],
  use: {
    actionTimeout: 0,
    trace: 'on-first-retry',
    headless: true
  },

  projects: [
    {
      name: 'chromium',
      testMatch: '/ui__tests/*.spec.ts',
      use: {
        baseURL: process.env.TEST_ENVIRONMENT == "prod" ? process.env.PROD_UI_URL : process.env.STAG_UI_URL,
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'api_tests',
      testMatch: '/api__tests/*.spec.ts',
      use: {
        baseURL: process.env.TEST_ENVIRONMENT == "prod" ? process.env.PROD_API_URL : process.env.STAG_API_URL,
        extraHTTPHeaders: {
          "Host": ""
        }
      }
    }
  ],
};

export default config;
